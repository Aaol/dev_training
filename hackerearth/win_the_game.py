import math
# https://www.hackerearth.com/practice/algorithms/dynamic-programming/introduction-to-dynamic-programming-1/practice-problems/algorithm/win-the-game/submissions/
# Write your code here
t = int(input())

for i in range(t):
    inputs = [int(x) for x in input().split()] 
    r, g = inputs[0], inputs[1]
    result = 0.0
    current_loose_prob = 1
    while True:
        if(r + g == 0):
            result = 1
            break
        else:
            result += ( r / (r + g) ) * current_loose_prob
            if g <= 1:
                if result == 0:
                    result = 1
                break
            current_loose_prob *= (g-1 ) / (r + g - 1)
            current_loose_prob *= (g ) / (r + g)
        g -= 2
    print('{:f}'.format(result))
